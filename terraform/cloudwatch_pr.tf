resource "aws_cloudwatch_event_rule" "pull_request_event" {
    name        = "capture-pull-request-lambda"
    description = "capture pull request to code commit repository for lambda function"

    event_pattern = <<EOF
{
  "source": [
    "aws.codecommit"
  ],
  "detail-type": [
    "CodeCommit Pull Request State Change"
  ],
  "resources": [
    "${aws_codecommit_repository.repo.arn}"
  ]
}
EOF
}

resource "aws_cloudwatch_event_target" "target_lambda_1" {
    rule = aws_cloudwatch_event_rule.pull_request_event.name
    arn  = aws_lambda_function.lambda_1.arn
}

resource "aws_cloudwatch_event_target" "target_codebuild" {
    rule     = aws_cloudwatch_event_rule.pull_request_event.name
    arn      = aws_codebuild_project.repo_build.arn
    role_arn = aws_iam_role.start_codebuild_role.arn

    input_transformer {
      input_paths = {
        sourceVersion     = "$.detail.sourceCommit",
        destinationCommit = "$.detail.destinationCommit",
        pullRequestId     = "$.detail.pullRequestId",
        repositoryName    = "$.detail.repositoryNames[0]",
        sourceCommit      = "$.detail.sourceCommit"
      }

      input_template = <<EOF
{
"sourceVersion": <sourceVersion>,
"artifactsOverride": {"type": "NO_ARTIFACTS"},
"environmentVariablesOverride": [
    {
      "name": "pullRequestId",
      "value": <pullRequestId>,
      "type": "PLAINTEXT"
    },
    {
      "name": "repositoryName",
      "value": <repositoryName>,
      "type": "PLAINTEXT"
    },
    {
      "name": "sourceCommit",
      "value": <sourceCommit>,
      "type": "PLAINTEXT"
    },
    {
      "name": "destinationCommit",
      "value": <destinationCommit>,
      "type": "PLAINTEXT"
    }
]
}
EOF
    }
}





