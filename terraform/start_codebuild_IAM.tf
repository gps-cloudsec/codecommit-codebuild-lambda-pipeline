resource "aws_iam_role" "start_codebuild_role" {
    name               = "start_codebuild_rule"
    assume_role_policy = data.aws_iam_policy_document.codebuild_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "pull_request_lambda_attachment_2" {
    role       = aws_iam_role.start_codebuild_role.name
    policy_arn = aws_iam_policy.codebuild_start_policy.arn
}

resource "aws_iam_policy" "codebuild_start_policy" {
    name = "codebuild_start_policy"
    policy = data.aws_iam_policy_document.start_codebuild_policy_doc.json
}

data "aws_iam_policy_document" "codebuild_assume_role_policy" {
    statement {
        effect = "Allow"

        actions = [
            "sts:AssumeRole"
        ]

        principals {
            type        = "Service"
            identifiers = ["events.amazonaws.com"]
        }
    }
}

data "aws_iam_policy_document" "start_codebuild_policy_doc" {
    statement {
        effect = "Allow"

        actions = [
            "codebuild:StartBuild"
        ]

        resources = ["*"]
    }
}