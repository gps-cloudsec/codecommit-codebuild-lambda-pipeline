resource "aws_iam_role" "lambda_role" {
    name               = "pull-request-comment-status-lambda-role"
    assume_role_policy = data.aws_iam_policy_document.lambda-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "lambda_attachment" {
    role       = aws_iam_role.lambda_role.name
    policy_arn = aws_iam_policy.write_comment_policy.arn
}

resource "aws_iam_role_policy_attachment" "lambda_attachment_2" {
    role = aws_iam_role.lambda_role.name
    policy_arn = aws_iam_policy.basic_lambda_policy.arn
}

resource "aws_iam_policy" "write_comment_policy" {
    name = "pull_request_comment_policy"
    policy = data.aws_iam_policy_document.pull_request_write_policy.json
}

resource "aws_iam_policy" "basic_lambda_policy" {
    name = "basic-lambda-policy"
    policy = data.aws_iam_policy_document.basic_lambda_policy_doc.json
}

data "aws_iam_policy_document" "lambda-assume-role-policy" {
    statement {
        effect = "Allow"

        actions = [
            "sts:AssumeRole"
        ]

        principals {
            type        = "Service"
            identifiers = ["lambda.amazonaws.com"]
        }
    }
}

data "aws_iam_policy_document" "pull_request_write_policy" {
    statement {
        effect = "Allow"

        actions = [
            "codecommit:PostCommentForPullRequest",
            "codecommit:UpdatePullRequestApprovalState",
            "codebuild:*",
        ]

        resources = [
            "*"
        ]
    }
}

data "aws_iam_policy_document" "basic_lambda_policy_doc" {
    statement {
        effect = "Allow"

        actions = [
            "logs:CreateLogGroup"
        ]

        resources = ["*"]
    }

    statement {
        effect = "Allow"

        actions = [
            "logs:CreateLogStream",
            "logs:PutLogEvents"
        ]

        resources = ["*"]
    } 
}