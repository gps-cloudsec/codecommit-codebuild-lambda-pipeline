/*
resource "aws_codepipeline" "codepipeline" {
    name = var.pipeline_name
    role_arn = aws_iam_role.codepipeline_role.arn

    artifact_store {
        location = aws_s3_bucket.codepipeline_bucket.bucket
        type     = "S3"
    }

    stage {
        name = "CodeCommit"

        action {
            name     = "TemplateSource"
            category = "Source"
            owner    = "AWS"
            provider = "CodeCommit"
            version = "1"

            configuration {
                RepositoryName = var.repository_name
                BranchName = "master"
                PollForSourceChanges = "false"
            }
        }
    }

    stage {
        name = "Build"

        action {
            name = "Validation"
            category = "Buid"
            owner = "AWS"
            provider = "Codebuild"
            version = "1"

            configuration {
                ProjectName = var.aws_coudebuild_project.repo_build.name
            }
        }
    }
}

resource "aws_iam_role" "codepipeline_role" {
  name = "test-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = "codepipeline-bucket"
  acl    = "private"
}
*/
