resource "aws_iam_role" "codebuild_service_role" {
    name = "codebuild-service-role"
    assume_role_policy = data.aws_iam_policy_document.codebuild_service_role_doc.json
}

resource "aws_iam_role_policy_attachment" "codebuild-policy-att" {
    role = aws_iam_role.codebuild_service_role.name
    policy_arn = aws_iam_policy.codebuild_policy.arn
}

resource "aws_iam_policy" "codebuild_policy" {
    name = "codebuild-service-policy"
    policy = data.aws_iam_policy_document.codebuild_service_policy_doc.json
}

data "aws_iam_policy_document" "codebuild_service_policy_doc" {
    statement {
        effect = "Allow"

        actions = [
            "s3:Put*",
            "s3:Get*",
            "logs:*",
            "codecommit:*"
        ]

        resources = ["*"]
    }
}

data "aws_iam_policy_document" "codebuild_service_role_doc" {
    statement {
        effect = "Allow"

        actions = [
            "sts:AssumeRole"
        ]

        principals {
            type        = "Service"
            identifiers = ["codebuild.amazonaws.com"]
        }
    }
}