resource "aws_codecommit_repository" "repo" {
  repository_name = var.repository_name
  description     = var.repository_name
  default_branch  = var.default_branch
}

resource "aws_iam_policy" "restrict_master_branch" {
    policy = data.aws_iam_policy_document.restrictmaster_policy.json
}

data "aws_iam_policy_document" "restrictmaster_policy" {
    statement {
        effect = "Allow"

        actions = [
            "codecommit:*"
        ]

        resources = [
            aws_codecommit_repository.repo.arn
        ]
    }

    statement {
        effect = "Deny"

        actions = [
            "codecommit:GitPush",
            "codecommit:DeleteBranch",
            "codecommit:PutFile",
            "codecommit:MergeBranchesByFastForward",
            "codecommit:MergeBranchesBySquash",
            "codecommit:MergeBranchesByThreeWay",
            "codecommit:MergePullRequestByFastForward",
            "codecommit:MergePullRequestBySquash",
            "codecommit:MergePullRequestByThreeWay"
        ]

        resources = [
            aws_codecommit_repository.repo.arn
        ]

        condition {
            test     = "StringEqualsIfExists"
            variable = "codecommit:References"
            values   = [
                "refs/heads/${var.default_branch}", 
                "refs/heads/${var.dev_branch_name}"
            ]
        }

        condition {
            test     = "Null"
            variable = "codecommit:References"
            values   = [false]
        }
    }
}