resource "aws_cloudwatch_event_rule" "codebuild_watch" {
    name        = "capture-codebuild-lambda"
    description = "get that status of code build to post comment on pull request if the build succeeded or failed."

    event_pattern = <<EOF
{
  "source": [
    "aws.codebuild"
  ],
  "detail-type": [
    "CodeBuild Build State Change"
  ],
  "detail": {
    "build-status": [
      "FAILED",
      "SUCCEEDED"
    ]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "target_lambda_2" {
    rule = aws_cloudwatch_event_rule.codebuild_watch.name
    arn  = aws_lambda_function.lambda_2.arn
}