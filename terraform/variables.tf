variable "repository_name" {
  type        = string
  description = "The name of your CodeCommit repository"
}

variable "repository_description" {
  type        = string
  description = "Description of your CodeCommit repository"
}

variable "default_branch" {
  type        = string
  description = "The name of the main/master repository branch"
}

variable "dev_branch_name" {
  type        = string
  description = "Name of the branch to use for development in the CodeCommit Repository (ex: dev). You will have to create this manually once the repository is deployed."
}

variable "compute_type" {
  default     = "BUILD_GENERAL1_SMALL"
  type        = string
  description = "Information about the compute resources the build project will use."
}

variable "image" {
  default     = "aws/codebuild/standard:1.0"
  type        = string
  description = "The image identifier of the Docker image to use for this build project."
}