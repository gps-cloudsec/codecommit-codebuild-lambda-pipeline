output "codecommit_repo_id" {
    value = aws_codecommit_repository.repo.repository_id
    description = "CodeCommit repository id"
}

output "codecommit_repo_http" {
    value = aws_codecommit_repository.repo.clone_url_http
    description = "URL to clone the repository over HTTPS"
}

output "codecommit_repo_ssh" {
    value = aws_codecommit_repository.repo.clone_url_ssh
    description = "URL to clone the repository over SSH"
}

output "codecommit_repo_name" {
    value = aws_codecommit_repository.repo.name
    description "Name of the CodeCommit repository"
}

output "codecommit_dev_branch" {
    value = var.dev_branch_name
    description = "Name of the development branch for CodeCommit"
}

output "codecommit_default_branch" {
    value = var.default_branch
    description "Name of the main/default branch for CodeCommit"
}

output "codebuild_project_name" {
    value = aws_codebuild_project.repo_build.name
    description = "CodeBuild project name"
}

