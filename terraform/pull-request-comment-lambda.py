import json
import boto3
import botocore
from botocore.exceptions import ClientError


def check_keys(passed_event):
    try:
        if passed_event["source"] != "aws.codecommit":
            return 0
        elif not (passed_event['detail']['event'] == "pullRequestCreated":
            return 0
        elif passed_event['detail-type'] != "CodeCommit Pull Request State Change":
            return 0
        elif passed_event['detail']['isMerged'] != "False":
            return 0
        elif not passed_event['detail']['pullRequestId']:
            return 0
        elif len(passed_event['detail']['repositoryNames']) == 0:
            return 0
    except KeyError as e:
        return 0

    return 1


def lambda_handler(event, context):
    if check_keys(event) != 1:
        return {
            'body': 'ERROR: Event json not valid.'
        }

    code_commit_client = boto3.client('codecommit')

    try:
        repo_name = event['detail']['repositoryNames'][0]
        pr_id = event['detail']['pullRequestId']
        source_id = event['detail']['sourceCommit']
        destination_id = event['detail']['destinationCommit']

        response = code_commit_client.post_comment_for_pull_request(
            pullRequestId=pr_id,
            repositoryName=repo_name,
            beforeCommitId=source_id,
            afterCommitId=destination_id,
            content='Code Build In Progress'
        )

        if response['comment']['commentId']:
            return {
                'body': 'Comment successful'
            }
        else:
            return {
                'body': 'Comment failed'
            }
    except ClientError as e:
        return {
            'body': '{}'.format(e.response['Error']['Code'])
        }
    except KeyError as e:
        return{
            'body': 'Comment failed'
        }



