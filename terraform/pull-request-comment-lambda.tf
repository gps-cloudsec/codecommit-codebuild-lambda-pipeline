resource "aws_lambda_function" "lambda_1" {
    function_name = "pullRequestCommentFunction"
    filename      = "lambda_functions/zips/pull-request-comment-lambda.zip"
    source_code_hash = data.archive_file.lambda_zip.output_base64sha256
    description   = "Comments on a PR with a build notification"
    runtime       = "python3.8"
    timeout       = 60
    role          = aws_iam_role.lambda_role.arn
    handler       = "pull-request-comment-lambda.lambda_handler"
}

resource "aws_lambda_permission" "allow_cloudwatch_2" {
  statement_id  = "AllowExecutionFromCloudWatch2"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_1.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.pull_request_event.arn
}

data "archive_file" "lambda_zip" {
    type          = "zip"
    source_file   = "pull-request-comment-lambda.py"
    output_path   = "lambda_functions/zips/pull-request-comment-lambda.zip"
}

