resource "aws_codebuild_project" "repo_build" {
    name          = "${var.repository_name}-build"
    description   = "CodeBuild for ${var.repository_name}"
    build_timeout = 5

    service_role = aws_iam_role.codebuild_service_role.arn

    artifacts {
        type = "NO_ARTIFACTS"
    }

    environment {
        compute_type                = var.compute_type
        image                       = var.image
        type                        = LINUX_CONTAINER
        image_pull_credentials_type = "CODEBUILD"
    }

    source {
        type = "CODECOMMIT"
        location = aws_codecommit_repository.repo.clone_url_http
        buildspec = file("buildspec.yml")
    }

    source_version = "refs/heads/${var.dev_branch_name}"
}


