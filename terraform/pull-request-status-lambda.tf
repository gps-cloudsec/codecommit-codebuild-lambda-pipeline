resource "aws_lambda_function" "lambda_2" {
    function_name = "pullRequestStatusFunction"
    filename      = "lambda_functions/zips/pull-request-status-lambda.zip"
    source_code_hash = data.archive_file.lambda_zip2.output_base64sha256
    description   = "Comments on a PR with the status from the associated codebuild project"
    runtime       = "python3.8"
    timeout       = 60
    role          = aws_iam_role.lambda_role.arn
    handler       = "pull-request-status-lambda.lambda_handler"
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_2.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.codebuild_watch.arn
}

data "archive_file" "lambda_zip2" {
    type          = "zip"
    source_file   = "pull-request-status-lambda.py"
    output_path   = "lambda_functions/zips/pull-request-status-lambda.zip"
}
