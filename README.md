# README #

Terraform deployment and Cloudformation template for the following pipeline. 
![Pipeline](./AWSNativeIaCCICD.png)

## Terraform Setup ##

* Clone the repository using `git clone`
  
* Navigate to the `terraform` directory
  
* Run `terraform apply`
    - The deployed CodeBuild resource uses a default `buildspec.yml` file that can be modified.
      - The `buildspec.yml` file is located in the CodeBuild resource, not the root directory of the CodeCommit respository. 

* Add code to the main branch of the CodeCommit repository
    - See more: `https://docs.aws.amazon.com/codecommit/latest/userguide/how-to-create-file.html`

* Create the development branch (`codecommit_dev_branch`) off of default/main branch (`codecommit_default_branch`). 
    - Create branch via AWS CLI: `https://docs.aws.amazon.com/cli/latest/reference/codecommit/create-branch.html`
    - Create branch via AWS Console or GIT: `https://docs.aws.amazon.com/codecommit/latest/userguide/how-to-create-branch.html`

* Once a pull request is created for the `codecommit_dev_branch` to the `codecommit_default_branch`, the pipeline will be initiated. 


### Inputs ###
| Variable                | Description 
| :---:                   |    :----:    
| repository_name         | The name of your CodeCommit repository      
| repository_description  | Description of your CodeCommit repository
| default_branch          | The name of the main/master repository branch      
| dev_branch_name         | Name of the branch to use for development in the CodeCommit Repository (ex: dev). You will have to create this manually once the repository is deployed.
| compute_type            | Information about the compute resources the build project will use.
| image                   | The image identifier of the Docker image to use for this build project.  

### Outputs ###
| Variable               | Description |
| :---:                  | :---:       
| codecommit_repo_id     | CodeCommit repository id
| codecommit_repo_http   | URL to clone the repository over HTTPS
| codecommit_repo_ssh    | URL to clone the repository over SSH 
| codecommit_repo_name   | Name of the CodeCommit repository
| codecommit_default_branch | Name of the main/default branch for CodeCommit
| codecommit_dev_branch  | Name of the development branch for CodeCommit
| codebuild_project_name | CodeBuild project name

## Cloudformation Setup ##

* Clone the repository using `git clone`

* Navigate to the `cloudformation` directory

* Run `aws cloudformation create-stack --stack-name NAME --template-body file://pipeline.yml --parameters ParameterKey=RepoName,ParameterValue=Value ParameterKey=RepoDescription,ParameterValue=Value ParameterKey=DefaultBranch,ParameterValue=Value ParameterKey=DevBranchName,ParameterKey=Value ParameterKey=ComputeType,ParameterValue=Value ParameterKey=Image,ParameterValue=Value`
    - See more: `https://docs.aws.amazon.com/cli/latest/reference/cloudformation/create-stack.html`
    - Create stack on AWS Console: `https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-create-stack.html`

* Add code to the main branch of the CodeCommit repository. 
  - See more: `https://docs.aws.amazon.com/codecommit/latest/userguide/how-to-create-file.html`

* Create the development branch (`DevBranchName`) off of default/main branch (`DefaultBranch`). 
    - Create branch via AWS CLI: `https://docs.aws.amazon.com/cli/latest/reference/codecommit/create-branch.html`
    - Create branch via AWS Console or GIT: `https://docs.aws.amazon.com/codecommit/latest/userguide/how-to-create-branch.html`

* Upload buildspec file to root branch (`DefaultBranch`) of CodeCommit repository

* Once a pull request is created for the `CodeCommitRepoDevBranch` to the `CodeCommitRepoDefaultBranch`, the pipeline will be initiated. 

### Inputs ### 
| Variable | Description | Allowed Values
| :---: | :---: | :---:
| RepoName | The name of your CodeCommit repository
| RepoDescription | A Description of your CodeCommit repository 
| DefaultBranch | Name of the default branch for your Codecommit repository
| DevBranchName | Name of the development branch for your repository (ex 'dev')
| ComputeType | The Compute Type to use for CodeBuild | BUILD_GENERAL1_SMALL, BUILD_GENERAL1_MEDIUM, BUILD_GENERAL1_LARGE
| Image | The Image to use for CodeBuild project | aws/codebuild/ubuntu-base:14.04, aws/codebuild/android-java-8:26.1.1, aws/codebuild/android-java-8:24.4.1, aws/codebuild/docker:17.09.0, aws/codebuild/golang:1.10, aws/codebuild/java:openjdk-8, aws/codebuild/java:openjdk-9, aws/codebuild/nodejs:10.1.0, aws/codebuild/nodejs:8.11.0, aws/codebuild/nodejs:6.3.1, aws/codebuild/php:5.6, aws/codebuild/php:7.0, aws/codebuild/python:3.6.5, aws/codebuild/python:3.5.2, aws/codebuild/python:3.4.5, aws/codebuild/python:3.3.6, aws/codebuild/python:2.7.12, aws/codebuild/ruby:2.5.1, aws/codebuild/ruby:2.3.1, aws/codebuild/ruby:2.2.5, aws/codebuild/dot-net:core-1, aws/codebuild/dot-net:core-2.0, aws/codebuild/windows-base:1.0

### Outputs ###
| Variable | Description 
| :---: | :---: 
| CodeCommitRepoHttp | Clone the CodeCommit repo via HTTP
| CodeCommitRepoSSH | Clone the CodeCommit repo via SSH
| CodeCommitRepoDefaultBranch | Name of the main branch in CodeCommit repo
| CodeCommitRepoDevBranch | Name of the dev branch in Codecommit repo_name
| CodeCommitRepoName | Name of the CodeCommit repo

